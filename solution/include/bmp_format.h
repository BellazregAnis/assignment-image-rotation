#ifndef BMP_FORMAT_H
#define BMP_FORMAT_H


#include <image.h>
#include <malloc.h>
#include <status_enum.h>

enum status from_bmp(FILE *in, struct image *image);

enum status to_bmp(FILE *out, const struct image *image);

#endif

