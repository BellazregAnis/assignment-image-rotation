#include "../include/image.h"
#include "../include/bmp_format.h"
#include "../include/file_service.h"
#include "../include/status_enum.h"
#include "../include/transformation.h"
#include <stdio.h>

int main(int argc, char **argv) {
    
    char *status_info[] = {
        [SUCCESS] = "The picture has been successfully flipped",
        [FILE_OPENING_ERROR] = "Could not open the file",
        [FILE_CLOSING_ERROR] = "Could not close the file",
        [READING_ERROR] = "Could not read from file",
        [WRITING_ERROR] = "Failed to write to file"
	};
 
    if (argc != 3) {
        printf("Invalid number of arguments\n");
        return 0;
    }

    FILE *input_file = {0};
    struct image first_image = {0};
    FILE *output_file = {0};
    
    const char* input_file_name = argv[1];
    const char* output_file_name = argv[2];

    enum status status = file_open(input_file_name, &input_file, "r");
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    status = from_bmp(input_file, &first_image);
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    status = file_close(input_file);
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    struct image second_image = transform(&first_image);
    image_destroy(first_image);

    status = file_open(output_file_name, &output_file, "w");
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    status = to_bmp(output_file, &second_image);
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    status = file_close(output_file);
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    image_destroy(second_image);
    printf("%s\n",status_info[SUCCESS]);

  
    return 0;
}
