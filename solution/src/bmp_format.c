#include "../include/bmp_format.h"
#include "../include/bmp_header.h"
#include "stdint.h"
#include <stdbool.h>


static bool read_header(FILE *file, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, file);
}

static uint32_t size_of_padding(const uint32_t width) {
    if (width % 4 == 0) return 0;
    return 4 - ((width * 3) % 4);
}

static size_t size_of_image(const struct image *image) {
    return image->height * (3 * image->width + size_of_padding(image->width));
}

static size_t size_of_file(const size_t image_size) {
    return image_size + sizeof(struct bmp_header);
}

enum status from_bmp(FILE *in, struct image *image) {
    struct bmp_header header = {0};
    if (!read_header(in, &header)) {
        return READING_ERROR;
    }

    *image = image_create(header.biWidth, header.biHeight);

    uint32_t i = 0;
    uint32_t j = 0;
    
    while (i < image->height){
    	while (j < image->width){
    		fread(&(image->data[image->width * i + j]), sizeof(struct pixel), 1, in);
    		j++;
    	}
    	fseek(in, (long) size_of_padding(image->width), SEEK_CUR);
    	i++;
    	j=0;
    }
    
    return SUCCESS;
}

enum status to_bmp(FILE *out, const struct image *image) {

    struct bmp_header header = {0};
   
    header.bfType = 19778;
    header.bfileSize = size_of_file(size_of_image(image));
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = size_of_image(image);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
	
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITING_ERROR;
    }


    const uint8_t zero = 0;
    
    size_t i = 0;
    size_t j = 0;
    
    if (image->data) {
        while (i < image->height){
        	fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out);
        	while (j < size_of_padding(image->width)){
        		fwrite(&zero, 1, 1, out);
        		j++;
        	}
       	i++;
        	j=0;
        }      
    } else {
        return WRITING_ERROR;
    }

    return SUCCESS;
}



